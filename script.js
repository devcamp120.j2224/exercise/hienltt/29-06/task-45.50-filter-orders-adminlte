$(document).ready(function(){    
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gOrderId = "";
    //biến toàn cục chứa thông tin đơn hàng
    var gDataOrders = [];
    const gORDER_COLS = ['orderId', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien', 'hoTen', 'soDienThoai', 'trangThai', 'action'];
    //Khai báo các cột của datatable
    const gCOLUMN_ORDER_ID = 0;
    const gCOLUMN_KICH_CO = 1;
    const gCOLUMN_LOAI_PIZZA = 2;
    const gCOLUMN_ID_lOAI_NUOC_UONG = 3;
    const gCOLUMN_THANH_TIEN = 4;
    const gCOLUMN_HO_TEN = 5;
    const gCOLUMN_SO_DIEN_THOAI = 6;
    const gCOLUMN_TRANG_THAI = 7;
    const gCOLUMN_ACTION = 8;
    //Định nghĩa table - chưa có data
    $('#table-order').DataTable({
        columns: [
            {data: gORDER_COLS[gCOLUMN_ORDER_ID]},
            {data: gORDER_COLS[gCOLUMN_KICH_CO]},
            {data: gORDER_COLS[gCOLUMN_LOAI_PIZZA]},
            {data: gORDER_COLS[gCOLUMN_ID_lOAI_NUOC_UONG]},
            {data: gORDER_COLS[gCOLUMN_THANH_TIEN]},
            {data: gORDER_COLS[gCOLUMN_HO_TEN]},
            {data: gORDER_COLS[gCOLUMN_SO_DIEN_THOAI]},
            {data: gORDER_COLS[gCOLUMN_TRANG_THAI]},
            {data: gORDER_COLS[gCOLUMN_ACTION]},
        ],
        //Ghi đè nội dung của cột action, chuyển thành button chi tiết
        columnDefs: [
            {
                targets: gCOLUMN_ACTION,
                defaultContent: '<button class="btn-detail btn btn-primary">Chi tiết</button>'
            }
        ]
    });
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // Gán event handler cho nút chi tiết
    $('#table-order').on('click', '.btn-detail', function(){
        onBtnDetailClick(this); // this là button được ấn 
    });

    // Gán event handler cho nút Lọc
    $('#btn-filter').on('click', function(){
        onBtnFilterDataClick();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
    // Hàm xử lý tải trang
    function onPageLoading(){
        "use strict";
        $.ajax({
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders',
            type: 'GET',
            dataType: 'json',
            success: function(res){
                gDataOrders = res;
                console.log(res);
                loadDataToTable(res);
            },
            error: function(error){
                alert(error.responseText);
            }
        });
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm xử lý ;load dữ liệu vào table
    function loadDataToTable(paramResponse){
        "use strict";
        var vUserTable = $('#table-order').DataTable();
        //Xoá toàn bộ dữ liệu đang có của bảng
        vUserTable.clear();
        //Cập nhật data cho bảng
        vUserTable.rows.add(paramResponse);
        //Cập nhật lại giao diện hiển thị của bảng
        vUserTable.draw();
    }
    // Hàm xử lý khi nhấn nút Chi tiết trong table
    function onBtnDetailClick(paramButton){
        "use strict";
        var vRowSelected = $(paramButton).closest('tr');
        var vTable = $('#table-order').DataTable();
        var vDataRow = vTable.row(vRowSelected).data();
        gId = vDataRow.id;
        gOrderId = vDataRow.orderId;
        console.log("ID: " + gId);
        console.log("Order ID: " + gOrderId);
    }
    // Hàm xử lý lọc dữ liệu
    function onBtnFilterDataClick(){
        'use strict';
        var vStatus = $('#select-stats').val();
        var vPizzaType = $('#select-pizza').val();
        var vDataFilter = gDataOrders.filter( function(paramOrder, index){
            return ((vStatus === "all" || vStatus.toUpperCase() === paramOrder.trangThai.toUpperCase())
            && (vPizzaType === 'all' || vPizzaType.toUpperCase() === paramOrder.loaiPizza.toUpperCase()))
        });
        // gọi hàm load dữ liệu vào bảng
        loadDataToTable(vDataFilter);
    }
})